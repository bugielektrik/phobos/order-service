CREATE TABLE IF NOT EXISTS orders (
    id                  SERIAL PRIMARY KEY,
    created_at          TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at          TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    order_id            VARCHAR NOT NULL UNIQUE,
    order_status        VARCHAR NOT NULL,
    customer_id         VARCHAR NOT NULL,
    store_id            VARCHAR NOT NULL,
    ship_name           VARCHAR NOT NULL,
    ship_description    VARCHAR NOT NULL,
    ship_latitude       VARCHAR NOT NULL,
    ship_longitude      VARCHAR NOT NULL,
    order_amount        NUMERIC NULL,
    payment_type        VARCHAR NULL,
    payment_status      VARCHAR NULL,
    payment_operation   VARCHAR NULL,
    courier_id          VARCHAR NULL,
    order_date          TIMESTAMP NULL,
    ship_date           TIMESTAMP NULL,
    accepted_time       NUMERIC NULL,
    arrived_time        NUMERIC NULL,
    progress_time       NUMERIC NULL,
    completed_time      NUMERIC NULL,
    overall_time        NUMERIC NULL,
    is_time_processed   BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE IF NOT EXISTS order_items (
    id          SERIAL PRIMARY KEY,
    created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    customer_id VARCHAR NOT NULL,
    order_id    VARCHAR NOT NULL,
    product_id  VARCHAR NOT NULL,
    quantity    NUMERIC NOT NULL,
    price       NUMERIC NOT NULL
);

CREATE TABLE IF NOT EXISTS order_history (
    id                  SERIAL PRIMARY KEY,
    created_at          TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at          TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    order_id            VARCHAR NOT NULL,
    order_status        VARCHAR NOT NULL,
    payment_type        VARCHAR NOT NULL,
    payment_status      VARCHAR NOT NULL,
    payment_operation   VARCHAR NOT NULL,
    payment_amount      VARCHAR NOT NULL,
    request_body        VARCHAR NULL,
    status_code         VARCHAR NULL,
    response_body       VARCHAR NULL,
    stage               NUMERIC NULL,
    time_difference     NUMERIC NULL
);