package api

import (
	"context"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"

	"gitlab.com/bugielektrik/phobos/order-service/internal/model"
)

type OrderService interface {
	Create(order *model.Order, customerID string) (*model.Order, error)
	GetAll(customerID string) ([]*model.Order, error)
	GetByID(id, customerID string) (*model.Order, error)
	Update(ctx context.Context, id, customerID string, order *model.OrderInstance) (*model.Order, error)
	Delete(id, customerID string) (*model.Order, error)
}

type Order struct {
	service OrderService
}

func NewOrderHandler(s OrderService) *Order {
	return &Order{service: s}
}

func (h *Order) createOrder(c *fiber.Ctx) error {
	// Bind params
	accountID := c.Get("X-Account-ID")

	orderDest := new(model.Order)
	if err := c.BodyParser(orderDest); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Validate params
	if err := validator.New().Struct(orderDest); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Write data to DB
	orderSrc, err := h.service.Create(orderDest, accountID)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	c.Status(fiber.StatusCreated)
	return c.JSON(orderSrc)
}

func (h *Order) getOrders(c *fiber.Ctx) error {
	// Bind params
	accountID := c.Get("X-Account-ID")

	// Read data from database
	ordersSrc, err := h.service.GetAll(accountID)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	return c.JSON(ordersSrc)
}

func (h *Order) getOrderByID(c *fiber.Ctx) error {
	// Bind params
	orderID := c.Params("id")
	accountID := c.Get("X-Account-ID")

	// Read data from database
	orderSrc, err := h.service.GetByID(orderID, accountID)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if orderSrc == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.JSON(orderSrc)
}

func (h *Order) updateOrder(c *fiber.Ctx) error {
	// Bind params
	orderID := c.Params("id")
	accountID := c.Get("X-Account-ID")

	// Bind request body
	orderDest := new(model.OrderInstance)
	if err := c.BodyParser(orderDest); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Write data to DB
	ctx := context.WithValue(c.Context(), "id", orderID)
	orderSrc, err := h.service.Update(ctx, orderID, accountID, orderDest)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if orderSrc == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.SendStatus(http.StatusOK)
}

func (h *Order) deleteOrder(c *fiber.Ctx) error {
	// Bind params
	orderID := c.Params("id")
	accountID := c.Get("X-Account-ID")

	// Delete data from DB
	orderSrc, err := h.service.Delete(orderID, accountID)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if orderSrc == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.SendStatus(http.StatusOK)
}
