package api

type Dependencies struct {
	ItemService  ItemService
	OrderService OrderService
}

type Handler struct {
	Item  *Item
	Order *Order
}

func New(d Dependencies) *Handler {
	return &Handler{
		Item:  NewItemHandler(d.ItemService),
		Order: NewOrderHandler(d.OrderService),
	}
}
