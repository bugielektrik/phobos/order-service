package api

import (
	"net/http"
	"time"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/bugielektrik/phobos/order-service/internal/config"
)

const idleTimeout = 5 * time.Second

func (h *Handler) InitRest(cfg *config.Config) *fiber.App {
	// Init rest api
	router := fiber.New(
		fiber.Config{
			IdleTimeout: idleTimeout,
		})

	//router.Use(
	//	cors.New(),
	//	helmet.New(),
	//	csrf.New(cfg.Csrf),
	//	limiter.New(cfg.Limiter),
	//	logger.New(),
	//)

	// Init router
	router.Get("/ping", func(c *fiber.Ctx) error {
		c.Status(http.StatusOK)
		return c.SendString("pong")
	})

	h.initRouter(router)

	return router
}

func (h *Handler) initRouter(router *fiber.App) {
	api := router.Group("/api/v1")
	{
		order := api.Group("/orders")
		{
			order.Post("", h.Order.createOrder)
			order.Post("", h.Order.getOrders)
			order.Post("/:id", h.Order.getOrderByID)
			order.Put("/:id", h.Order.updateOrder)
			order.Delete("/:id", h.Order.deleteOrder)
		}
		item := api.Group("/items")
		{
			item.Post("", h.Item.createItem)
			item.Post("", h.Item.getItems)
			item.Post("/:id", h.Item.getItemByID)
			item.Put("/:id", h.Item.updateItem)
			item.Delete("/:id", h.Item.deleteItem)
		}
	}
}
