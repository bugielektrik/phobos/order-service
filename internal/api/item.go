package api

import (
	"context"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"

	"gitlab.com/bugielektrik/phobos/order-service/internal/model"
)

type ItemService interface {
	Create(item []*model.Item, customerID string) ([]*model.Item, error)
	GetAll(customerID string) ([]*model.Item, error)
	GetByID(id, customerID string) (*model.Item, error)
	Update(ctx context.Context, id, customerID string, item *model.ItemInstance) (*model.Item, error)
	Delete(id, customerID string) (*model.Item, error)
}

type Item struct {
	service ItemService
}

func NewItemHandler(s ItemService) *Item {
	return &Item{service: s}
}

func (h *Item) createItem(c *fiber.Ctx) error {
	// Bind params
	accountID := c.Get("X-Account-ID")

	itemsDest := make([]*model.Item, 0)
	if err := c.BodyParser(itemsDest); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	for _, itemDest := range itemsDest {
		// Validate params
		if err := validator.New().Struct(itemDest); err != nil {
			c.Status(fiber.StatusBadRequest)
			return c.JSON(fiber.Map{"error": err.Error()})
		}
	}

	// Write data to DB
	itemsSrc, err := h.service.Create(itemsDest, accountID)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	c.Status(fiber.StatusCreated)
	return c.JSON(itemsSrc)
}

func (h *Item) getItems(c *fiber.Ctx) error {
	// Bind params
	accountID := c.Get("X-Account-ID")

	// Read data from database
	itemsSrc, err := h.service.GetAll(accountID)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	return c.JSON(itemsSrc)
}

func (h *Item) getItemByID(c *fiber.Ctx) error {
	// Bind params
	orderID := c.Params("id")
	accountID := c.Get("X-Account-ID")

	// Read data from database
	itemSrc, err := h.service.GetByID(orderID, accountID)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if itemSrc == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.JSON(itemSrc)
}

func (h *Item) updateItem(c *fiber.Ctx) error {
	// Bind params
	orderID := c.Params("id")
	accountID := c.Get("X-Account-ID")

	// Bind request body
	itemDest := new(model.ItemInstance)
	if err := c.BodyParser(itemDest); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Write data to DB
	ctx := context.WithValue(c.Context(), "id", orderID)
	itemSrc, err := h.service.Update(ctx, orderID, accountID, itemDest)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if itemSrc == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.SendStatus(http.StatusOK)
}

func (h *Item) deleteItem(c *fiber.Ctx) error {
	// Bind params
	orderID := c.Params("id")
	accountID := c.Get("X-Account-ID")

	// Delete data from DB
	itemSrc, err := h.service.Delete(orderID, accountID)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if itemSrc == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.SendStatus(http.StatusOK)
}
