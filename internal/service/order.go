package service

import (
	"context"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"

	"gitlab.com/bugielektrik/phobos/order-service/internal/model"
)

type OrderStore interface {
	Create(data *model.Order) error
	Get(id string) (*model.Order, error)
	GetAll(customerID string) ([]*model.Order, error)
	GetByID(id, customerID string) (*model.Order, error)
	Delete(id, customerID string) error
	Lock(ctx context.Context, id, customerID string) (func(), *sqlx.Tx, error)
	Update(ctx context.Context, tx *sqlx.Tx, id, customerID string, data *model.OrderInstance) error
}

type Order struct {
	store OrderStore
}

func NewOrderService(s OrderStore) *Order {
	return &Order{store: s}
}

func (s *Order) Create(orderDest *model.Order, customerID string) (*model.Order, error) {
	// Generate data ID
	for {
		id := uuid.New().String()

		orderSrc, err := s.store.Get(id)
		if err != nil {
			return nil, err
		}

		if orderSrc == nil {
			orderDest.ID = id
			break
		}
	}

	orderDest.OrderStatus = "created"
	orderDest.CustomerID = customerID

	err := s.store.Create(orderDest)
	return orderDest, err
}

func (s *Order) GetAll(customerID string) ([]*model.Order, error) {
	ordersSrc, err := s.store.GetAll(customerID)
	return ordersSrc, err
}

func (s *Order) GetByID(id, customerID string) (*model.Order, error) {
	orderSrc, err := s.store.GetByID(id, customerID)
	return orderSrc, err
}

func (s *Order) Update(ctx context.Context, id, customerID string, orderDest *model.OrderInstance) (*model.Order, error) {
	orderSrc, err := s.store.GetByID(id, customerID)
	if err != nil {
		return nil, err
	}

	if orderSrc == nil {
		return nil, nil
	}

	unlockOrder, reqTx, err := s.store.Lock(ctx, id, customerID)
	if err != nil {
		return nil, err
	}
	defer unlockOrder()

	err = s.store.Update(ctx, reqTx, id, customerID, orderDest)
	if err != nil {
		return nil, err
	}

	return orderSrc, nil
}

func (s *Order) Delete(id, customerID string) (*model.Order, error) {
	orderSrc, err := s.store.GetByID(id, customerID)
	if err != nil {
		return nil, err
	}

	if orderSrc == nil {
		return nil, nil
	}

	err = s.store.Delete(id, customerID)
	if err != nil {
		return nil, err
	}

	return orderSrc, nil
}
