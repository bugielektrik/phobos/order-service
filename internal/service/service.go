package service

type Dependencies struct {
	ItemStore  ItemStore
	OrderStore OrderStore
}

type Service struct {
	Item  *Item
	Order *Order
}

func New(d Dependencies) *Service {
	return &Service{
		Item:  NewItemService(d.ItemStore),
		Order: NewOrderService(d.OrderStore),
	}
}
