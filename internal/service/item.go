package service

import (
	"context"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"

	"gitlab.com/bugielektrik/phobos/order-service/internal/model"
)

type ItemStore interface {
	Create(data *model.Item) error
	Get(id string) (*model.Item, error)
	GetAll(customerID string) ([]*model.Item, error)
	GetByID(id, customerID string) (*model.Item, error)
	Delete(id, customerID string) error
	Lock(ctx context.Context, id, customerID string) (func(), *sqlx.Tx, error)
	Update(ctx context.Context, tx *sqlx.Tx, id, customerID string, data *model.ItemInstance) error
}

type Item struct {
	store ItemStore
}

func NewItemService(s ItemStore) *Item {
	return &Item{store: s}
}

func (s *Item) Create(itemsDest []*model.Item, customerID string) ([]*model.Item, error) {
	for _, itemDest := range itemsDest {
		// Generate data ID
		for {
			id := uuid.New().String()

			itemSrc, err := s.store.Get(id)
			if err != nil {
				return nil, err
			}

			if itemSrc == nil {
				itemDest.ID = id
				break
			}
		}

		itemDest.CustomerID = customerID

		err := s.store.Create(itemDest)
		if err != nil {
			return nil, err
		}
	}

	return itemsDest, nil
}

func (s *Item) GetAll(customerID string) ([]*model.Item, error) {
	itemsSrc, err := s.store.GetAll(customerID)
	return itemsSrc, err
}

func (s *Item) GetByID(id, customerID string) (*model.Item, error) {
	itemSrc, err := s.store.GetByID(id, customerID)
	return itemSrc, err
}

func (s *Item) Update(ctx context.Context, id, customerID string, itemDest *model.ItemInstance) (*model.Item, error) {
	itemSrc, err := s.store.GetByID(id, customerID)
	if err != nil {
		return nil, err
	}

	if itemSrc == nil {
		return nil, nil
	}

	unlockItem, reqTx, err := s.store.Lock(ctx, id, customerID)
	if err != nil {
		return nil, err
	}
	defer unlockItem()

	err = s.store.Update(ctx, reqTx, id, customerID, itemDest)
	if err != nil {
		return nil, err
	}

	return itemSrc, nil
}

func (s *Item) Delete(id, customerID string) (*model.Item, error) {
	itemSrc, err := s.store.GetByID(id, customerID)
	if err != nil {
		return nil, err
	}

	if itemSrc == nil {
		return nil, nil
	}

	err = s.store.Delete(id, customerID)
	if err != nil {
		return nil, err
	}

	return itemSrc, nil
}
