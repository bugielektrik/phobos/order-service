package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/jmoiron/sqlx"

	"gitlab.com/bugielektrik/phobos/order-service/internal/model"
)

type Order struct {
	db     *sqlx.DB
	logger hclog.Logger
}

func NewOrderStore(db *sqlx.DB, logger hclog.Logger) *Order {
	return &Order{
		db:     db,
		logger: logger,
	}
}

func (s *Order) Create(data *model.Order) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := s.db.NamedExecContext(ctx, `
		INSERT INTO orders (order_id, order_amount, order_status, customer_id, store_id, ship_name, ship_description, ship_latitude, ship_longitude, payment_type, payment_status, payment_operation, courier_id, order_date, ship_date) 
		VALUES(:order_id, :order_amount, :order_status, :customer_id, :store_id, :ship_name, :ship_description, :ship_latitude, :ship_longitude, :payment_type, :payment_status, :payment_operation, :courier_id, :order_date, :ship_date)`,
		data)
	return err
}

func (s *Order) Get(id string) (*model.Order, error) {
	query := `
		SELECT order_id
		FROM orders
		WHERE order_id=:order_id`
	args := map[string]interface{}{
		"order_id": id,
	}
	data, err := s.getRow("Get", query, args)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Order) GetByID(id, customerID string) (*model.Order, error) {
	query := `
		SELECT order_id, order_amount, order_status, customer_id, store_id, ship_name, ship_description, ship_latitude, ship_longitude, payment_type, payment_status, payment_operation, courier_id, order_date, ship_date
		FROM orders
		WHERE order_id=:order_id AND customer_id=:customer_id`
	args := map[string]interface{}{
		"order_id":    id,
		"customer_id": customerID,
	}
	data, err := s.getRow("GetByID", query, args)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Order) GetAll(customerID string) ([]*model.Order, error) {
	query := `
		SELECT order_id, order_amount, order_status, customer_id, store_id, ship_name, ship_description, ship_latitude, ship_longitude, payment_type, payment_status, payment_operation, courier_id, order_date, ship_date
		FROM orders
		WHERE customer_id='` + customerID + `'`
	data, err := s.getRows("GetAll", query)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Order) Delete(id, customerID string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := s.db.NamedExecContext(ctx, `
		DELETE FROM orders
		WHERE order_id=:order_id AND customer_id=:customer_id`,
		map[string]interface{}{
			"order_id":    id,
			"customer_id": customerID,
		})
	return err
}

func (s *Order) Update(ctx context.Context, tx *sqlx.Tx, id, customerID string, data *model.OrderInstance) error {
	setValues := make([]string, 0)

	if data.OrderStatus != nil {
		setValues = append(setValues, "order_status=:order_status")
	}

	if data.CourierID != nil {
		setValues = append(setValues, "courier_id=:courier_id")
	}

	if data.ShipDate != nil {
		setValues = append(setValues, "ship_date=:ship_date")
	}

	setValues = append(setValues, "updated_at=CURRENT_TIMESTAMP")

	setQuery := strings.Join(setValues, ", ")
	query := fmt.Sprintf("UPDATE orders SET %s WHERE order_id='%s' AND customer_id='%s'", setQuery, id, customerID)

	err := s.update(ctx, tx, "Update", query, data)
	return err
}

func (s *Order) Lock(ctx context.Context, id, customerID string) (func(), *sqlx.Tx, error) {
	logger := s.logger.With("operation", "Lock", "id", id)

	query := `SELECT * FROM orders WHERE order_id=:order_id AND customer_id=:customer_id FOR UPDATE`
	args := map[string]interface{}{
		"order_id":    id,
		"customer_id": customerID,
	}

	logger.Debug("begin transaction")
	tx, err := s.db.BeginTxx(ctx, nil)
	if err != nil {
		logger.Error("failed to begin transaction", "error", err)
		return nil, nil, err
	}

	rows, err := tx.NamedQuery(query, args)
	if err != nil {
		logger.Error("failed to execute statement", "error", err)
		return nil, nil, err
	}
	defer rows.Close()

	logger.Debug("locked")
	return func() {
		err := tx.Commit()
		if err != nil {
			logger.Error("error while unlocking", "error", err)
			return
		}

		logger.Debug("unlocked")
	}, tx, nil
}

func (s *Order) getRow(operation, query string, args interface{}) (*model.Order, error) {
	logger := s.logger.With("operation", operation)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	row, err := s.db.NamedQueryContext(ctx, query, args)
	if err != nil {
		logger.Error("failed to get", "error", err)
		return nil, err
	}
	defer row.Close()

	if !row.Next() {
		logger.Debug("not found")
		return nil, nil
	}

	data := new(model.Order)
	err = row.StructScan(&data)
	if err != nil {
		logger.Error("failed to scan into struct", "error", err)
		return nil, err
	}

	return data, nil
}

func (s *Order) getRows(operation, query string) ([]*model.Order, error) {
	logger := s.logger.With("operation", operation)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rows, err := s.db.QueryxContext(ctx, query)
	if err == sql.ErrNoRows {
		logger.Debug("not found")
		return nil, nil
	} else if err != nil {
		logger.Error("failed to get", "error", err)
		return nil, err
	}
	defer rows.Close()

	dataArr := make([]*model.Order, 0)
	for rows.Next() {
		data := new(model.Order)
		err = rows.StructScan(&data)
		if err != nil {
			logger.Error("failed to scan into struct", "error", err)
			return nil, err
		}
		dataArr = append(dataArr, data)
	}

	return dataArr, nil
}

func (s *Order) update(ctx context.Context, tx *sqlx.Tx, operation, query string, args interface{}) error {
	logger := s.logger.With("operation", operation)

	logger.Debug("start exec")
	finalize := func() error { return nil }
	if tx == nil {
		ttx, err := s.db.BeginTxx(ctx, nil)
		if err != nil {
			// failed to start transaction
			logger.Error("failed to start transaction", "error", err)
			return err
		}
		finalize = func() error {
			err = ttx.Commit()
			if err != nil {
				logger.Error("failed to commit", "error", err)
				return err
			}

			logger.Debug("committed update")
			return nil
		}
		tx = ttx
	}

	_, err := tx.NamedExec(query, args)
	if err != nil {
		logger.Error("failed to update", "error", err)
		return err
	}

	logger.Info("updated")
	return finalize()
}
