package postgres

import (
	"github.com/hashicorp/go-hclog"
	"github.com/jmoiron/sqlx"
)

type Store struct {
	Item  *Item
	Order *Order
}

func New(db *sqlx.DB, logger hclog.Logger) *Store {
	return &Store{
		Item:  NewItemStore(db, logger),
		Order: NewOrderStore(db, logger),
	}
}
