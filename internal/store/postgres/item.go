package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/jmoiron/sqlx"

	"gitlab.com/bugielektrik/phobos/order-service/internal/model"
)

type Item struct {
	db     *sqlx.DB
	logger hclog.Logger
}

func NewItemStore(db *sqlx.DB, logger hclog.Logger) *Item {
	return &Item{
		db:     db,
		logger: logger,
	}
}

func (s *Item) Create(data *model.Item) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := s.db.NamedExecContext(ctx, `
		INSERT INTO order_items (item_id, customer_id, order_id, product_id, quantity, price) 
		VALUES(:item_id, :customer_id, :order_id, :product_id, :quantity, :price)`,
		data)
	return err
}

func (s *Item) Get(id string) (*model.Item, error) {
	query := `
		SELECT item_id
		FROM order_items
		WHERE item_id=:item_id`
	args := map[string]interface{}{
		"item_id": id,
	}
	data, err := s.getRow("Get", query, args)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Item) GetByID(id, customerID string) (*model.Item, error) {
	query := `
		SELECT item_id, customer_id, order_id, product_id, quantity, price
		FROM order_items
		WHERE item_id=:item_id AND customer_id=:customer_id`
	args := map[string]interface{}{
		"item_id":     id,
		"customer_id": customerID,
	}
	data, err := s.getRow("GetByID", query, args)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Item) GetAll(customerID string) ([]*model.Item, error) {
	query := `
		SELECT item_id, customer_id, order_id, product_id, quantity, price
		FROM order_items
		WHERE customer_id='` + customerID + `'`
	data, err := s.getRows("GetAll", query)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Item) Delete(id, customerID string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := s.db.NamedExecContext(ctx, `
		DELETE FROM order_items
		WHERE item_id=:item_id AND customer_id=:customer_id`,
		map[string]interface{}{
			"item_id":     id,
			"customer_id": customerID,
		})
	return err
}

func (s *Item) Update(ctx context.Context, tx *sqlx.Tx, id, customerID string, data *model.ItemInstance) error {
	setValues := make([]string, 0)

	if data.Quantity != nil {
		setValues = append(setValues, "quantity=:quantity")
	}

	setValues = append(setValues, "updated_at=CURRENT_TIMESTAMP")

	setQuery := strings.Join(setValues, ", ")
	query := fmt.Sprintf("UPDATE order_items SET %s WHERE item_id='%s' AND customer_id='%s'", setQuery, id, customerID)

	err := s.update(ctx, tx, "Update", query, data)
	return err
}

func (s *Item) Lock(ctx context.Context, id, customerID string) (func(), *sqlx.Tx, error) {
	logger := s.logger.With("operation", "Lock", "id", id)

	query := `SELECT * FROM order_items WHERE item_id=:item_id AND customer_id=:customer_id FOR UPDATE`
	args := map[string]interface{}{
		"item_id":     id,
		"customer_id": customerID,
	}

	logger.Debug("begin transaction")
	tx, err := s.db.BeginTxx(ctx, nil)
	if err != nil {
		logger.Error("failed to begin transaction", "error", err)
		return nil, nil, err
	}

	rows, err := tx.NamedQuery(query, args)
	if err != nil {
		logger.Error("failed to execute statement", "error", err)
		return nil, nil, err
	}
	defer rows.Close()

	logger.Debug("locked")
	return func() {
		err := tx.Commit()
		if err != nil {
			logger.Error("error while unlocking", "error", err)
			return
		}

		logger.Debug("unlocked")
	}, tx, nil
}

func (s *Item) getRow(operation, query string, args interface{}) (*model.Item, error) {
	logger := s.logger.With("operation", operation)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	row, err := s.db.NamedQueryContext(ctx, query, args)
	if err != nil {
		logger.Error("failed to get", "error", err)
		return nil, err
	}
	defer row.Close()

	if !row.Next() {
		logger.Debug("not found")
		return nil, nil
	}

	data := new(model.Item)
	err = row.StructScan(&data)
	if err != nil {
		logger.Error("failed to scan into struct", "error", err)
		return nil, err
	}

	return data, nil
}

func (s *Item) getRows(operation, query string) ([]*model.Item, error) {
	logger := s.logger.With("operation", operation)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rows, err := s.db.QueryxContext(ctx, query)
	if err == sql.ErrNoRows {
		logger.Debug("not found")
		return nil, nil
	} else if err != nil {
		logger.Error("failed to get", "error", err)
		return nil, err
	}
	defer rows.Close()

	dataArr := make([]*model.Item, 0)
	for rows.Next() {
		data := new(model.Item)
		err = rows.StructScan(&data)
		if err != nil {
			logger.Error("failed to scan into struct", "error", err)
			return nil, err
		}
		dataArr = append(dataArr, data)
	}

	return dataArr, nil
}

func (s *Item) update(ctx context.Context, tx *sqlx.Tx, operation, query string, args interface{}) error {
	logger := s.logger.With("operation", operation)

	logger.Debug("start exec")
	finalize := func() error { return nil }
	if tx == nil {
		ttx, err := s.db.BeginTxx(ctx, nil)
		if err != nil {
			// failed to start transaction
			logger.Error("failed to start transaction", "error", err)
			return err
		}
		finalize = func() error {
			err = ttx.Commit()
			if err != nil {
				logger.Error("failed to commit", "error", err)
				return err
			}

			logger.Debug("committed update")
			return nil
		}
		tx = ttx
	}

	_, err := tx.NamedExec(query, args)
	if err != nil {
		logger.Error("failed to update", "error", err)
		return err
	}

	logger.Info("updated")
	return finalize()
}
