package model

import (
	"time"

	"github.com/shopspring/decimal"
)

type Order struct {
	InternalID       string          `json:"-" db:"id"`
	ID               string          `json:"order_id" db:"order_id"`
	OrderStatus      string          `json:"order_status" db:"order_status"`
	CustomerID       string          `json:"customer_id" db:"customer_id"`
	StoreID          string          `json:"store_id" db:"store_id" validate:"required"`
	ShipName         string          `json:"ship_name" db:"ship_name" validate:"required"`
	ShipDescription  string          `json:"ship_description" db:"ship_description" validate:"required"`
	ShipLatitude     decimal.Decimal `json:"ship_latitude,omitempty" db:"ship_latitude" validate:"required"`
	ShipLongitude    decimal.Decimal `json:"ship_longitude,omitempty" db:"ship_longitude" validate:"required"`
	OrderAmount      decimal.Decimal `json:"order_amount" db:"order_amount"`
	PaymentType      string          `json:"payment_type" db:"payment_type"`
	PaymentStatus    string          `json:"payment_status" db:"payment_status"`
	PaymentOperation string          `json:"payment_operation" db:"payment_operation"`
	CourierID        string          `json:"courier_id" db:"courier_id"`
	OrderDate        time.Time       `json:"order_date" db:"order_date"`
	ShipDate         time.Time       `json:"ship_date" db:"ship_date"`
	AcceptedTime     decimal.Decimal `json:"accepted_time" db:"accepted_time"`
	ArrivedTime      decimal.Decimal `json:"arrived_time" db:"arrived_time"`
	ProgressTime     decimal.Decimal `json:"progress_time" db:"progress_time"`
	CompletedTime    decimal.Decimal `json:"completed_time" db:"completed_time"`
	OverallTime      decimal.Decimal `json:"overall_time" db:"overall_time"`
	IsTimeProcessed  string          `json:"is_time_processed" db:"is_time_processed"`
	CreatedAt        time.Time       `json:"created_at" db:"created_at"`
	UpdatedAt        time.Time       `json:"updated_at" db:"updated_at"`
}

type OrderInstance struct {
	OrderStatus *string    `json:"order_status" db:"order_status"`
	CourierID   *string    `json:"courier_id" db:"courier_id"`
	ShipDate    *time.Time `json:"ship_date" db:"ship_date"`
}
