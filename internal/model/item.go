package model

import (
	"time"

	"github.com/shopspring/decimal"
)

type Items struct {
	Items []Item `json:"items" db:"items"`
}

type Item struct {
	InternalID string          `json:"-" db:"id"`
	ID         string          `json:"item_id" db:"item_id"`
	CustomerID string          `json:"customer_id" db:"customer_id"`
	OrderID    string          `json:"order_id" db:"order_id" validate:"required"`
	ProductID  string          `json:"product_id" db:"product_id" validate:"required"`
	Price      decimal.Decimal `json:"price" db:"price" validate:"required"`
	Quantity   decimal.Decimal `json:"quantity" db:"quantity" validate:"required"`
	CreatedAt  time.Time       `json:"created_at" db:"created_at"`
	UpdatedAt  time.Time       `json:"updated_at" db:"updated_at"`
}

type ItemInstance struct {
	Quantity *decimal.Decimal `json:"quantity" db:"quantity"`
}
